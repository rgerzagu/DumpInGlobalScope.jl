module DumpInGlobalScope

export @dump
"""
@dump
Export all internal function variables into Main scope as gobal variables
>> a
ERROR: UndefVarError: a not defined
>> function test()
    a = 12
    end
>> test()
>> a
ERROR: UndefVarError: a not defined
>> function test2()
    a = 12
    b = "coucou"
    @dump 
    end
>> test()
>> a
    a = 12
>> b
    "coucou"
"""
macro dump()
    quote
        # Getting all local variables
        d = Base.@locals
        # Export them in workspace
        for (key,val) in d
            exp = :(global $key = $val)
            eval(exp)
        end
    end
end

"""
@dump  var1, var2, ...
Same as @dump call but only export variables specified in the macro call.
>> function test2()
    a = 12
    b = "coucou"
    @dump a
    end
>> test()
>> a
    a = 12
>> b
    ERROR: UndefVarError: b not defined
"""
macro dump(vars...)
    # Container for expression
    blk = Expr(:block)
    for variable in vars
        # Escape value to align dump and local values
        local value = esc(variable)
        # Set the expression to have dump variables
        push!(blk.args,:(global $variable = $value))
    end
    return blk
end

end

using DumpInGlobalScope
using Test

@testset "Export all variables" begin
    # Check a is not defined 
    @test_throws UndefVarError a
    # We do not export a, so a should still be unknown 
    function testWoDump()
        a = 12
    end
    testWoDump()
    @test_throws UndefVarError a
    # Export a with a the only variables in the function 
    function testWithDump() 
        a = 12 
        b = "coucou"
        @dump a
    end
    testWithDump()
    @test a == 12
    @test_throws UndefVarError b
end



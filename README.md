# DumpInGlobalScope.jl


Export all local variables (or all specified variables) from function scope to workspace 

## Purpose 

In Julia function are more efficient and powerful to use than scripts. It allows to have code efficiency and debugging tools such as `Debugger.jl`and `Infiltrator.jl`.  But scripts are more easy to use for protoptyping purpose such as plotting data, experiment and so forth.

## The workaround 

This package proposes a simple macro to switch from function scope to global scope. It allows to have every variable directly in global scope. In this way, the default script mode should be a function with it last line with `@dump`. All the local variable are now accessible from the `REPL`.

Example: to bump all variables 
```
   >> a
ERROR: UndefVarError: a not defined
>> function test()
    a = 12
    end
>> test()
>> a
ERROR: UndefVarError: a not defined
>> function test2()
    a = 12
    b = "coucou"
    @dump
    end
>> test()
>> a
    a = 12
>> b
    "coucou"
```

Example to dump only some variables 
```
   >> a
ERROR: UndefVarError: a not defined
>> function test()
    a = 12
    end
>> test()
>> a
ERROR: UndefVarError: a not defined
>> function test2()
    a = 12
    b = "coucou"
    @dump a
    end
>> test()
>> a
    a = 12
>> b
ERROR: UndefVarError: b not defined
```


## Why it may be useful …

Let's start with a simple script, assuming fresh Julia session.
```
   # Define a vector in global scope
   N = 100
   x = randn(1,N)
   y = randn(1,N)
   for (n,x) = enumerate(x)
     x[n] = n + x
     y[n] = n - x + y
   end
   println(x)
```

If we want to debug the `for` loop we have to use a function. But then we have to set the return parameters of the function to `x` and `y`.  

```
   function test()
      # Define a vector in global scope
      N = 100
      x = randn(1,N)
      y = randn(1,N)
      for (n,x) = enumerate(x)
        x[n] = n + x
        y[n] = n - x + y
      end
      return (x,y)
    end
``` 
The more we have parameters and variable to examine, the more it overweights the functions.
The workaround of the package is to use `@dump` or `@dump x` to export in global scope the desired (or all) variables. It brings closer the function based debugger and the scripting part.

## and why you shouldn't use it anyway 

The macro is not in Julia philosophy, it breaks macro hygiene, puts stuff in global scope... This is very bad. Consider this macro as a hacky/trashy way to wait for efficient debugging tools in scripts.



